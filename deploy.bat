@rem deploy to PyPI
@rem get mui version
@for /f usebackq %%i in (`python tools\mui_version.py`) do (@set MUIVER=%%i)
@echo VERSION=%MUIVER%
python setup.py sdist
twine upload --repository pypi dist/marlowe_ui-%MUIVER%.tar.gz

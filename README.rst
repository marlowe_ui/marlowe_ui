==============================================
Marlowe User Interface (marlowe_ui)
==============================================

Takaaki AOKI (aoki.takaaki@nagoya-u.jp)

About Malouwe User Interface
=============================

Marlowe User Interface is support program for marlowe (a binary collision simulator, http://www.oecd-nea.org/tools/abstract/detail/psr-0137/)
to desgin input data, as well as parse output .lst data.

Programs and scripts
--------------------

- mui (mui.py, mui.exe):
    GUI program to construct marlowe input file

- ml_post_ui (ml_post_ui.exe or ml_post_ui.py):
    GUI program to parse output data.

- ml_post (ml_post.exe or ml_post.py):
    Command line program to parse output data.

Project URLs
================

https://bitbucket.org/marlowe_ui/marlowe_ui/

Documents
  https://bitbucket.org/marlowe_ui/marlowe_ui/wiki/

Windows Binary
  https://bitbucket.org/marlowe_ui/marlowe_ui/downloads/

  (download *marlowe_ui-(version)-win-amd64.exe*)

PyPI
   https://pypi.python.org/pypi/marlowe_ui

@echo off
@rem cmdline option
@set setupopt=-q
@if "%1" == "-v" set setupopt=-v
@if "%1" == "/v" set setupopt=-v

@rem Inno setup environment
@set INNO="C:\Program Files (x86)\Inno Setup 6"

@rem Cleaump *~ files
@for /f usebackq %%i in (`dir /B /S marlowe_ui ^| findstr "~$"`) do (del %%i)

@rem get mui version
@for /f usebackq %%i in (`python tools\mui_version.py`) do (@set MUIVER=%%i)
@echo VERSION=%MUIVER%
@for /f usebackq %%i in (`python tools\cxf_platform_info.py`) do (@set WINBUILD=%%i)
@set WINBUILD=%WINBUILD%

@rem create .rst.tmpl -> .rst -> .html
@rem tools\gen_readme.py README.tmpl README.rst
rst2html.py marlowe_ui\examples\mui_compat\README.rst marlowe_ui\examples\mui_compat\README.html
cd marlowe_ui\doc
@call make.bat
cd ..\..
cd doc
@call make.bat
cd ..

@rem create standalone package using cx_freeze
python setup_cx.py %setupopt% build_exe
@rem assert existence of tcl86t.dll and tk86t.dll 
if not exist %WINBUILD%\lib\tcl86t.dll (
  echo "cannnot find tcl86t.dll"
  exit /B 0
)
if not exist %WINBUILD%\lib\tk86t.dll (
  echo "cannnot find tk86t.dll"
  exit /B 0
)
@rem run INNO setup to bundle executables
python setup_iss.py setup_template.iss setup.iss
%INNO%\iscc.exe setup.iss

@rem web pabe contents
if not exist html mkdir html
if not exist html\dist mkdir html\dist
copy doc\*.html html
for %%i in (-win32.exe, -win-amd64.exe) do (if exist dist\marlowe_ui-%MUIVER%%%i copy dist\marlowe_ui-%MUIVER%%%i html\dist)

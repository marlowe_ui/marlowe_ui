import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui.spawn_diag import SpawnDialog

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

logger = logging.getLogger('marlowe_ui')

d = SpawnDialog(app)
logger.info(d.result)

d = SpawnDialog(app, cmdline='preset value', input='argument', workdir=r'C:\somw\path')
logger.info(d.result)

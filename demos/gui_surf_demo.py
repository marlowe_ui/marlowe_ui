import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import logging

import tkinter as tk

from marlowe_ui.contrib.bundlepmw import Pmw

from marlowe_ui import guidata
from marlowe_ui import gui_surf
from marlowe_ui import logconsole
from marlowe_ui.tktool import gui_checkframe

logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()
Pmw.initialise(app)

gui_checkframe.gui_checkframe(app, gui_surf.Surf, guidata.surf_example)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

app.mainloop()

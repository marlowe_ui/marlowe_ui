import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui import logconsole

import logging
logging.basicConfig(level=logging.NOTSET)

from marlowe_ui import axis
from marlowe_ui.tktool import codedoptionmenu

logger = logging.getLogger('marlowe_ui')

app = tk.Tk()

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

demoframe = tk.Frame(app)
demoframe.pack(expand=True, fill=tk.BOTH)

w = axis.Axis(demoframe)
w.pack(expand=True, fill=tk.Y)

opframe = tk.Frame(app)
opframe.pack(side=tk.TOP, expand=False, fill=tk.Y)

# get
tk.Button(opframe, text='get', command=lambda: logger.info(w.get())).pack(side=tk.LEFT)

# set
tk.Label(opframe, text='set value').pack(side=tk.LEFT)
options = [
    (0, 'Default Param'),
    (1, 'Example Param 1')]
values = {
    0: axis.param_default,
    1: axis.param_example}
setbtn = codedoptionmenu.CodedOptionMenu(opframe)
setbtn.set_new_option(options, command=lambda val: w.set(values[setbtn.get()]))
setbtn.pack(side=tk.LEFT)

# clear
tk.Button(opframe, text='clean', command=lambda: w.clear()).pack(side=tk.LEFT)

# validate
tk.Button(opframe, text='validate', command=lambda: logger.info(w.validate())).pack(side=tk.LEFT)

# enable/disable  toggle
cv = tk.IntVar(opframe, True)
tk.Checkbutton(opframe, text='en/disable', variable=cv,
               command=lambda: w.enable() if cv.get() else w.disable()).pack(side=tk.LEFT)

app.mainloop()

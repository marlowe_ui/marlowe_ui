import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui.contrib.bundlepmw import Pmw

from marlowe_ui import guidata
from marlowe_ui import gui_atomtbl
from marlowe_ui.tktool import gui_checkframe

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()
Pmw.initialise(app)

gui_checkframe.gui_checkframe(
    app, gui_atomtbl.AtomTbl,
    guidata.atom_tbl_example)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

app.mainloop()

import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui import logconsole

import logging
logging.basicConfig(level=logging.NOTSET)

from marlowe_ui.tktool import validateentry
from marlowe_ui.tktool import codedoptionmenu
from marlowe_ui.tktool import optionframe

app = tk.Tk()


class MyOptionFrame(optionframe.OptionFrame):
    def body(self):
        w = validateentry.Vec3d(self)
        w.pack(expand=True)
        return w

demoframe = tk.Frame(app)
demoframe.pack(expand=True, fill=tk.BOTH)

w = MyOptionFrame(demoframe, title='enable option')
w.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

opframe = tk.Frame(app)
opframe.pack(side=tk.TOP, expand=False, fill=tk.Y)

# get
tk.Button(opframe, text='get', command=lambda: logger.info(w.get())).pack(side=tk.LEFT)

# set
tk.Label(opframe, text='set value').pack(side=tk.LEFT)
options = [
    (0, 'None'),
    (1, '[1, 2, 3, 4]')]
values = {
    0: None,
    1: [1, 2, 3, 4]}
setbtn = codedoptionmenu.CodedOptionMenu(opframe)
setbtn.set_new_option(options, command=lambda val: w.set(values[setbtn.get()]))

setbtn.pack(side=tk.LEFT)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')
logger = logging.getLogger('marlowe_ui')

app.mainloop()

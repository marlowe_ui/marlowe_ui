import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import json
import codecs

import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox

from marlowe_ui.contrib.bundlepmw import Pmw

from marlowe_ui import guidata
from marlowe_ui import to_marlowe
from marlowe_ui import layoutmode
from marlowe_ui import balloonhelp

from marlowe_ui.gui_root import Root
from marlowe_ui.tktool import codedoptionmenu
from marlowe_ui.tktool import error

from marlowe_ui import logconsole

import logging
logging.basicConfig(level=logging.NOTSET)

if __name__ == '__main__':
    # layout gui and test
    app = tk.Tk()
    Pmw.initialise(app)

    root = Root(app)
    root.pack(side=tk.TOP)

    debugframe = tk.Frame(app)

    # test buttons
    def set_action():
        root.set(guidata.root_example)

    setbtn = tk.Button(debugframe, text='set example', command=set_action)
    setbtn.pack(side=tk.LEFT)

    def clear_action():
        root.clear()

    clearbtn = tk.Button(debugframe, text='clear', command=clear_action)
    clearbtn.pack(side=tk.LEFT)

    def get_action():
        print(root.get())

    getbtn = tk.Button(debugframe, text='get', command=get_action)
    getbtn.pack(side=tk.LEFT)

    def val_action():
        print(root.validate())

    valbtn = tk.Button(debugframe, text='validate', command=val_action)
    valbtn.pack(side=tk.LEFT)

    def load_action():
        stream = tkinter.filedialog.askopenfile(
            title=u'Load json file', defaultextension='.json',
            filetypes=[('JSON', '*.json'), ('All', '*')], mode='rb')
        if stream:
            # load json format
            stream = codecs.getreader('utf-8')(stream)
            d = json.load(stream)
            root.set(d)
    loadbtn = tk.Button(debugframe, text='load .json', command=load_action)
    loadbtn.pack(side=tk.LEFT)

    def save_action():
        class VExecption(Exception):
            def __init__(self, err):
                Exception.__init__(self)
                self.err = err

        try:
            err = root.validate()
            if err:
                raise VExecption(err)
            d = root.get()
        except VExecption as e:
            tkinter.messagebox.showerror(
                'Validation error',
                'validation error, save is aborted.\n\n' + error.format_errorstruct(e.err))
            return
        except Exception as e:
            tkinter.messagebox.showerror(
                'exception error', 'exception received, save is aborted.\n'+e)
            return

        fname = tkinter.filedialog.asksaveasfilename(
            title=u'Save marlowe (&json) control file', filetypes=[('All', '*')])
        if fname:
            # save as marlowe format
            stream = open(fname, 'wt')
            to_marlowe.to_marlowe(d, stream)
            # save as json format
            stream = open(fname+u'.json', 'wb', encoding='utf-8')
            json.dump(d, stream, indent=4, sort_keys=True)

    getandsavebtn = tk.Button(debugframe, text='save', command=save_action)
    getandsavebtn.pack(side=tk.LEFT)

    # layout test
    if isinstance(root, layoutmode.LayoutModeImplement):
        def layout_action(v):
            mode = layout.get()
            root.layout(mode)
        layout = codedoptionmenu.CodedOptionMenu(
            debugframe, layoutmode.modes, command=layout_action)
        tk.Label(debugframe, text='Layout:').pack(side=tk.LEFT, padx=(2, 0), pady=2)
        layout.pack(side=tk.LEFT, padx=(0, 2), pady=2)
        layout.set(0)

    debugframe.pack(side=tk.TOP)

    # balloon help
    if isinstance(root, balloonhelp.BalloonHelpImplement):
        balloon = Pmw.Balloon(app)
        root.bind_with_balloonhelp(balloon)

    logwin = logconsole.getLogConsoleWindow('marlowe_ui')

    app.mainloop()

import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui import vpar

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()

demoframe = tk.Frame(app)
demoframe.pack(expand=True, fill=tk.BOTH)

v = vpar.Vpar(demoframe)
v.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

opframe = tk.Frame(app)
opframe.pack(side=tk.TOP, expand=False, fill=tk.Y)

# get
tk.Button(opframe, text='get', command=lambda: logger.info(v.get())).pack(side=tk.LEFT)

# set example
tk.Button(opframe, text='set example',
          command=lambda: v.set(vpar.param_example)).pack(side=tk.LEFT)

# clear
tk.Button(opframe, text='clear', command=lambda: v.clear()).pack(side=tk.LEFT)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')
logger = logging.getLogger('marlowe_ui')

app.mainloop()

import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

from marlowe_ui import guidata
from marlowe_ui import to_marlowe

to_marlowe.to_marlowe(guidata.app_example, sys.stdout)
# to_marlowe.to_marlowe(guidata.app_default, sys.stdout)

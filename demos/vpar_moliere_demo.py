import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui import scon

from marlowe_ui import vpar_moliere

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()

demoframe = tk.Frame(app)
demoframe.pack(expand=True, fill=tk.BOTH)

v = vpar_moliere.VparMoliere(demoframe)
v.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

opframe = tk.Frame(app)
opframe.pack(side=tk.TOP, expand=False, fill=tk.Y)

# get
tk.Button(opframe, text='get', command=lambda: logger.info(v.get())).pack(side=tk.LEFT)

# set example
tk.Button(opframe, text='set example',
          command=lambda: v.set(vpar_moliere.param_example)).pack(side=tk.LEFT)

# clear
tk.Button(opframe, text='clear', command=lambda: v.clear()).pack(side=tk.LEFT)

# enable
tk.Button(opframe, text='enable', command=lambda: v.enable()).pack(side=tk.LEFT)
# disable
tk.Button(opframe, text='disable', command=lambda: v.disable()).pack(side=tk.LEFT)

# validate
tk.Button(opframe, text='validate', command=lambda: v.validate()).pack(side=tk.LEFT)

# set_validkinds
tk.Label(opframe, text='kind:').pack(side=tk.LEFT)
k = tk.StringVar(opframe, str(scon.kind))
kinds = tk.OptionMenu(
    opframe, k,
    *[str(i+1) for i in range(scon.kind)],
    command=lambda val: v.set_validkinds(int(val)))
kinds.pack(side=tk.LEFT)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')
logger = logging.getLogger('marlowe_ui')

app.mainloop()

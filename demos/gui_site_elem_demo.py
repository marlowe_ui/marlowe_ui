import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui.contrib.bundlepmw import Pmw

import marlowe_ui
from marlowe_ui import guidata
from marlowe_ui import gui_site_elem
import marlowe_ui.tktool.gui_checkframe

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()
Pmw.initialise(app)

marlowe_ui.tktool.gui_checkframe.gui_checkframe(app, gui_site_elem.SiteElem,
                                                guidata.site_elem_example)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

app.mainloop()

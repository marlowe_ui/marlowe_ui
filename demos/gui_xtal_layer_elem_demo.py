import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui.contrib.bundlepmw import Pmw

import marlowe_ui
from marlowe_ui import guidata
from marlowe_ui import logconsole

from marlowe_ui.gui_xtal_layer_elem import XtalLayerElem
import marlowe_ui.tktool.gui_checkframe


import logging
logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger('marlowe_ui')


# test button frame extends from marlowe_ui.tktool.gui_checkframe.CheckButtonFrame
class CheckButtonFrame(marlowe_ui.tktool.gui_checkframe.CheckButtonFrame):
    def __init__(self, master, gui, example=None):
        marlowe_ui.tktool.gui_checkframe.CheckButtonFrame.__init__(self, master,
                                                                   gui, example)

        surfoptframe = tk.Frame(self)

        def enable_surfopt_action():
            gui.enable_surfopt()

        esurfbtn = tk.Button(surfoptframe, text='enable surfopt',
                             command=enable_surfopt_action)
        esurfbtn.pack(side=tk.LEFT, pady=2)

        def disable_surfopt_action():
            gui.disable_surfopt()

        dsurfbtn = tk.Button(surfoptframe, text='disable surfopt',
                             command=disable_surfopt_action)
        dsurfbtn.pack(side=tk.LEFT, pady=2)

        surfoptframe.pack()


app = tk.Tk()
Pmw.initialise(app)

xtal_layer_elem = XtalLayerElem(app)

c = CheckButtonFrame(app, xtal_layer_elem, guidata.xtal_layer_elem_default)

xtal_layer_elem.pack()
c.pack()

if isinstance(xtal_layer_elem, marlowe_ui.balloonhelp.BalloonHelpImplement):
    xtal_layer_elem.bind_with_balloonhelp(Pmw.Balloon(app))

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

app.mainloop()

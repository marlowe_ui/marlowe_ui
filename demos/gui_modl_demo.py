import sys
import os
sys.path.insert(0, os.path.join(__file__, '../..'))

import tkinter as tk

from marlowe_ui import guidata
from marlowe_ui import gui_modl
from marlowe_ui.tktool import gui_checkframe

from marlowe_ui import logconsole
import logging
logging.basicConfig(level=logging.NOTSET)

app = tk.Tk()

gui_checkframe.gui_checkframe(app, gui_modl.Modl, guidata.modl_example)

logwin = logconsole.getLogConsoleWindow('marlowe_ui')

app.mainloop()

import sys
import os

import pathlib
import itertools

ver_file = os.path.join(os.path.dirname(__file__), 'marlowe_ui', 'version.py')
vars = {}
exec(open(ver_file).read(), vars)

# set {TCL,TK}_LIBRARY
# see https://stackoverflow.com/questions/34939356/python-pygame-exe-build-with-cx-freeze-tcl-library-error 
#     https://stackoverflow.com/questions/43464355/how-to-find-the-path-of-tcl-tk-library-that-tkinter-is-currently-using?rq=1 
import tkinter
root = tkinter.Tk()
tcl_library_path = pathlib.Path(root.tk.exprstring('$tcl_library'))
tk_library_path = pathlib.Path(root.tk.exprstring('$tk_library'))
os.environ['TCL_LIBRARY'] = str(tcl_library_path)
os.environ['TK_LIBRARY'] = str(tk_library_path)

# find tk86t.dll and tcl86t.dll
tcltk_search_dll = {'tk86t.dll', 'tcl86t.dll'}
tcltk_found_dll = {}

for p in sys.path:
    p = pathlib.Path(p)
    for f in p.glob('*'):
        if f.is_file():
            f_low = f.name.lower()
            if f_low in tcltk_search_dll:
                tcltk_found_dll[f_low] = str(f)
                tcltk_search_dll.remove(f_low)

                if not tcltk_search_dll:
                    break
    else:
        continue
    break

if tcltk_search_dll:
    raise 'cannot find tcl/tk DLL. ' + str(tcltk_search_dll)
print('tcl/tk DLL:', tcltk_found_dll)


from cx_Freeze import setup, Executable

# def create_include_tix():
#     """ find tixxx.dll and related files, manually
#     see http://www.py2exe.org/index.cgi/TixSetup
#     """
#     # get tcl/tk install information
#     import tkinter
#     import _tkinter
#
#     tk = _tkinter.create()
#     # get tcl, tk, tix versions
#     tcl_version = _tkinter.TCL_VERSION
#     tk_version = _tkinter.TK_VERSION
#     tix_version = tk.call('package', 'version', 'Tix')
#
#     # get tcl library directory
#     tcl_dir = tk.call('info', 'library')
#
#     del tkinter, _tkinter
#
#     print('tcl_version = ', tcl_version)
#     print('tk_version = ', tk_version)
#     print('tix_version = ', tix_version)
#     print('tcl_dir = ', tcl_dir)
#
#     # try to find tix directory
#     tix_basename = 'tix{}'.format(tix_version)
#     tix_dir = os.path.join(os.path.dirname(tcl_dir), tix_basename)
#     print('tix_dir = ', tix_dir)
#     if os.path.isdir(tix_dir):
#         return [(tix_dir, tix_basename)]
#     else:
#         print('tix_dir does not exist.')
#
#     return []


def create_include_doc():
    docpath = pathlib.Path('doc')
    htmls = docpath.glob('*.html')
    rsts = docpath.glob('*.rst')
    fs = []
    for f in itertools.chain(htmls, rsts):
        fs.append((str(f), str(f)))
    return fs


def create_example_doc():
    fs = []
    fs.append(('marlowe_ui/examples/mui_compat/README.rst', 'examples/mui_compat/README.rst'))
    fs.append(('marlowe_ui/examples/mui_compat/README.html', 'examples/mui_compat/README.html'))

    examplepath = pathlib.Path('marlowe_ui/examples')
    dats = examplepath.glob('**/*.dat')
    jsons = examplepath.glob('**/*.json')
    lsts = pathlib.Path('marlowe_ui/examples/postprocess').glob('**')
    for f in itertools.chain(dats, jsons, lsts):
        dst = pathlib.PurePath(*(f.parts[1:]))
        fs.append((str(f), str(dst)))
    return fs

library_package_data = [
    ('marlowe_ui/postprocess/skelton/summary.pvsm',
     'library_package_data/marlowe_ui/postprocess/skelton/summary.pvsm'),
    ('marlowe_ui/postprocess_lark/skelton/summary.pvsm',
     'library_package_data/marlowe_ui/postprocess_lark/skelton/summary.pvsm')]

tcltk_libraries = [(v, 'lib/'+k) for k, v in tcltk_found_dll.items()]

print(tcltk_libraries)

include_files = create_include_doc() + create_example_doc() + library_package_data + tcltk_libraries
# modules?
includes = []
excludes = []
# 3rd party packages it would be better than using requires given by distutils?
packages = ['marlowe_ui', 'lockfile']

# we need to include tix libs explicitly
build_exe_options = {
    'include_files': include_files,
    'includes': includes,
    'excludes': excludes,
    'packages': packages}

setup(
    name="marlowe_ui",
    version=vars['__version__'],
    description="UI program for Marlowe input data file",
    long_description=open('README.rst').read(),
    author="Takaaki AOKI",
    author_email="aoki.takaaki@nagoya-u.jp",
    url="https://bitbucket.org/marlowe_ui/marlowe_ui/",
    download_url="https://bitbucket.org/marlowe_ui/marlowe_ui/",
    # cx_Freeze option
    options={'build_exe': build_exe_options},
    executables=[Executable('mui.py', base='Win32GUI'),
                 Executable('ml_post.py', base='Console'),
                 Executable('ml_post_ui.py', base='Win32GUI'),
                 Executable('ml_update_guidata.py', base='Console')])

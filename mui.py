#!/usr/bin/env python

import logging
logging.basicConfig(level=logging.NOTSET)

import marlowe_ui.run

marlowe_ui.run.run()

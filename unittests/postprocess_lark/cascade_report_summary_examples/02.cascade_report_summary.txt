Summary of Cascade   668:     Group  668     Number  1
.....Total number of.....                               .....Total number of.....
Collisions                          5199
Atoms available for pairing          341                Sites available for pairing          368
Pairs identified                     341                Pair separations > VCR               259
Unpaired vacant sites                 27                Atoms escaping all surfaces           16
Atoms trapped at all surfaces          8                Replacement sequences                405
Focuson sequences                     61                Truncated trajectories                 4
Beheaded replacement sequences         0                Beheaded focuson sequences             0
Redisplaced sequence members           0                Other redisplaced targets              0
Redisplacements, distant pairs         0                Redisplaced adatoms                    0
Multiple redisplacements               0
Targets rejected in SKATR: T/E Spectrum (channel width 0.1)               Total
Lattice (collisions)      0      0      5      5     10          20     48     35     26     11         160
Distribution of Repeated Target Searches
1         2         3         4         5              6         7         8         9        10+           Sum
341        74        16         6         3              2         0         0         1         2            445
Analysis of Time Ordering
Misordered vacant sites    156                  Misordered nonlattice targets      0
Ordered vacant sites       669                  Ordered nonlattice targets         0
Time Order of Collisions
Histogram scale (fs)  1024.00         Origin in channel    1     First reported channel    1     Entries     5199
Negative time values    69
4495       0       0       0       0             0       0       0       0       0
1     634
Frequencies of Simultaneous Target and Vacancy Encounters
RB        Number of              0        1        2        3        4        5        6        7        8        9
1   Lattice targets           349     4150      459        4        0        0        0        0        0        0
Vacant lattice sites     4267      471      168       44        9        1        0        1        0        0
2   Lattice targets            16      521       65        0        0        0        0        0        0        0
Vacant lattice sites      557       38        4        2        1        0        0        0        0        0
Frequencies of Simultaneous Target and Vacancy Encounters
RB        Number of             10       11
1   Lattice targets             0        0
Vacant lattice sites        0        1
2   Lattice targets             0        0
Vacant lattice sites        0        0
Distribution of Sequences of Nondisplacive Collisions Occurring Above Replacement Threshold
Total entries      94      Origin in Channel 0      First reported channel    1     Channel width 1
64     18      7      4      0             1

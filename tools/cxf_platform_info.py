# see cx_Freeze/dist.py 

import distutils.util
import sys
import os

build_base = 'build'
dir_name = "exe.%s-%s" % \
                    (distutils.util.get_platform(), sys.version[0:3])

print(os.path.join(build_base, dir_name))

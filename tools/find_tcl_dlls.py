import sys
import os
import pathlib

to_be_found = {'tk86t.dll', 'tcl86t.dll'}
found = {}

for p in sys.path:
    p = pathlib.Path(p)
    for f in p.glob('*'):
        if f.is_file():
            f_low = f.name.lower()
            if f_low in to_be_found:
                found[f_low] = str(f)
                to_be_found.remove(f_low)
                if not to_be_found:
                    break
    else:
        continue
    break

print('to_be_found', to_be_found)
print('found', found)

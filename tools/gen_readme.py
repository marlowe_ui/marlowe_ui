#!/usr/bin/env python

"""embed version and packaing date into README template
"""

import sys
import string
import argparse
import datetime

import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parents[1]))

from marlowe_ui import version

mui_version = version.__version__
build_datestr = datetime.date.today().strftime("%Y-%m-%d")


def gen_readme(infile, outfile):
    outfile.write(
        string.Template(infile.read()).substitute(
            mui_version=mui_version,
            build_datestr=build_datestr))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'infile', type=argparse.FileType('rt'), nargs='?',
        default=sys.stdin, help='input template file (default: stdin)')
    parser.add_argument(
        'outfile', type=argparse.FileType('wt'), nargs='?',
        default=sys.stdout, help='output file (default: stdout)')

    args = parser.parse_args()

    gen_readme(args.infile, args.outfile)

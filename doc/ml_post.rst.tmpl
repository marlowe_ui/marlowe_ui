======================================================
ml_post - postprocessing MARLOW output file
======================================================

:Author: Takaaki AOKI <aoki.takaaki@nagoya-u.jp>
:Version: ${mui_version}
:Date: ${build_datestr}

.. contents:: Contents
   :depth: 1

Usage (gui version)
===================

For windows, find ``Data Postprocessing (ml_post_ui.exe)`` in ``marlowe_ui`` group and click it.
Or, type ``ml_post_ui`` or ``ml_post_ui.py`` on console. 

On GUI, select .lst file to be expanded and push ``Expand`` button.

.. note::

  1. input filename should have '.lst' suffix. ex. marlowe_result.lst

  2. output directory is created at same directory as input data, with '.post' suffix
     (ex. marlowe_result.post corresponding to avobe input filename).

Usage (command line version)
=============================

To run postprocesseor,

.. code-block:: shell

  $$ ml_post marlowe_result.lst

, or 

.. code-block:: shell

  $$ ml_post.py marlowe_result.lst

``marlowe_result.lst`` is the output data by MARLOWE.  ml_post.py parses
``marlowe_result.lst`` and extracts some contents into ``marlowe_result.post``
directory (the output directory is automatically created at same directory of
input file). 

Data Preview using ParaView
============================

In order to preview the parsed data, a state file for ParaView (http://www.paraview.org)
is provided as ``marlowe_result.post/summary.pvsm``. To review it, select ``File -> Load State``
to load summary.pvsm file, or, from command line, 

.. code-block:: shell

  $$ paraview --state=summary.psvm


Syntax
------

::

  usage: ml_post.py [-h] [--cascade-directory-format CASCADE_DIRECTORY_FORMAT]  
                    input [output]                                              
                                                                                
  positional arguments:                                                         
    input                 Input file. '-' for stdin                             
    output                output data directory to output parsed data. If       
                          ommitted, INPUT.post is set for INPUT.lst data file. If
                          input is stdin, this option should be given.          
                                                                                
  optional arguments:                                                           
    -h, --help            show this help message and exit
    --cascade-directory-format CASCADE_DIRECTORY_FORMAT
                          directory form to store data for each cascade, which
                          is created under OUTPUT directory
                          (default: casc{Cascade:05d}-gr{Group:05d}-num{Number:05d})
    -l {DEBUG,INFO,WARNING,ERROR,CRITICAL}, --logging {DEBUG,INFO,WARNING,ERROR,CRITICAL}
                          choose logging level (default : INFO)
    --skip-verbose-textblock-output
                            skip output of verbose text block
    --cascade-table-output {BUNDLE,SEPARATE,BOTH}
                          Select output form for large cascade data tables
                          (ex. description_of_cascade{_all}.csv) (default: BUNDLE)
                            BUNDLE: output as <root>/xxx_all.csv
                            SEPARATE: output as <root>/CASCADE_DIRECTORY_FORMAT/xxx.csv
                            BOTH: BUNDLE and SEPARATE
    --ignore-block-parse-error
    			  ignore errors during parsing each text block. This is useful
			  to test whether the blockseparater runs correctly to the end.

    --old-inline-parser   use old inline parser

Output Data Files
==================

The structure of output data is following. Each file is optional and might be
missing depending on the configuration of output file.

::

  |
  +- RESULT.lst
  |      Marlowe Output File (input file for ml_post.py)
  |
  +- RESULT.post/
      |    Output directory generated by ml_post.py
      |
      +- initial.json
      |      JSON data of parsed object (initial configuration part).
      |
      +- summary.json
      |      JSON data of parsed object (initial and final summary part).
      |
      +- summary.pvsm
      |      Paraview state file to preview results.
      |
      +- analysis_of_primary_recoil_ranges.csv
      |      Table from "Analysis of Primary Recoil Ranges ( ##### Stopped Particles)"
      |      section.
      |
      +- statistical_analysis_of_data.csv
      |      Table from "Statistical Analysis of Data from  ##### Cascades" section.
      |
      +- description_of_cascade_all.csv
      |      Concatenated data of description_of_cascade.csv for each cascade detail.
      |      (available when --cascade-table-output == {BUNDLE or BOTH}, similar for the below *_all.csv files)
      |
      +- description_of_cascade_primary_recoil_all.csv
      |      Concatenated data of description_of_cascade_primary_recoil.csv for each cascade detail.
      |
      +- distant_iv_pairs_all.csv
      |      Concatenated data of distant_iv_pairs.csv for each cascade detail.
      |
      +- lattice_sites_all.csv
      |      Concatenated data of lattice_sites.csv for each cascade detail.
      |
      +- primary_recoil_ranges_all.csv
      |      Concatenated data of primary_recoil_ranges.csv for each cascade detail.
      |
      +- 00.initial_config.txt
      |      Raw text block data describing configuration data
      |      (this files is always output regardless of --skip-verbose-textblock-output)
      |
      +- 00.final_rangex.txt
      |      (suppressed when --skip-verbose-textblock-output is set, similar for the below 00.*.txt)
      |
      +- 00.final_slavex.txt
      |
      |
      +- 00.final_slavex_atom.txt
      |
      |
      +- 00.final_seqex.txt
      |
      |
      +- 00.final_orgex.txt
      |
      |
      +- casc#####-gr####-num####/
               Detail data for each cascade section. The section starts like,
               "Cascade     1:     Group    1      Number  1"

The content of each cascade directory is ...

::

  |
  +- RESULT.lst
  |
  +- RESULT.post/
      |    
      +- casc#####-gr#####-num#####/
          |
          +- cascade.json
          |      JSON packed data of cascade specific information
          |
          +- description_of_cascade.csv
          |      Table entitled with "Detailed Description of the Cascade part {1,2,3} of 3",
          |      which includes the initial and final position and other states of the
          |      knocked-on atoms
          |      (available when --cascade-table-output == {SEPARATE or BOTH}, similar for the below .csv files)
          |
          +- description_of_cascade_primary_recoil.csv
          |      The first line content of description_of_cascade.csv
          |      
          +- distant_iv_pairs.csv
          |      Table entitles with "Separations of Distant Interstitial-Vacancy Pairs".
          |      Interstitial and vacancy file numbers are related with those in
          |      description_of_cascade.csv and lattice_sites.csv.
          |      The coordinates of int. and vac. are also shown in this file.
          |
          +- initial_defect_atom.csv
          |      Rasidual defect atoms before collision
          |
          +- initial_defect_lattice.csv
          |      Rasidual defect lattice sites before collision
          |
          +- lattice_sites.csv
          |      Table entitled with "Locations of the Cascade Lattice Sites", which includes
          |      the coordinate of lattice sites involved into the entire cascade process.
          |
          +- primary_recoil_ranges.csv
          |      Taken from "XX Primary Recoil Ranges" section, simple result on final status
          |      of primary recoil
          |
          +- 00.cascade_summary.txt
          |      Raw text data block starts with 'Cascade{d6}:     Group{d5}     Number{d3}'
          |      (suppressed when --skip-verbose-textblock-output is set, similar for the below 00.*.txt)
          |
          +- 00.cascade_detail_initial_defect_lattice.txt
          |
          +- 00.cascade_detail_initial_defect_atom.txt
          |
          +- 00.cascade_detail_projectile.txt
          |
          +- 00.cascade_report_summary.txt
          |
          +- 00.cascade_report_atom.txt
          |
          +- 00.cascade_report_detail_1.txt
          |
          +- 00.cascade_report_detail_2.txt
          |
          +- 00.cascade_report_detail_3.txt
          |
          +- 00.cascade_report_lattice_sites.txt
          |
          +- 00.cascade_rangex.txt
          |
          +- 00.cascade_sequin.txt
          |
          +- 00.cascade_orgiv.txt


The old output form (with --old-inline-parser) is following

::

  |
  +- RESULT.lst
  |      Marlowe Output File (input file for ml_post.py)
  |
  +- RESULT.post
      |    Output directory generated by ml_post.py
      |
      +- analysis_of_primary_recoil_ranges.csv
      |      Table from "Analysis of Primary Recoil Ranges ( ##### Stopped Particles)"
      |      section.
      |
      +- description_of_cascade_all.csv
      |      Concatenated data of description_of_cascade.csv for each cascade detail.
      |
      +- distant_iv_pairs_all.csv
      |      Concatenated data of distant_iv_pairs.csv for each cascade detail.
      |
      +- lattice_sites_all.csv
      |      Concatenated data of lattice_sites.csv for each cascade detail.
      |
      +- primary_recoil_ranges_all.csv
      |      Concatenated data of primary_recoil_ranges.csv for each cascade detail.
      |
      +- statistical_analysis_of_data.csv
      |      Table from "Statistical Analysis of Data from  ##### Cascades" section.
      |
      +- summary.json
      |      JSON packed data of parsed object.
      |
      +- summary.pvsm
      |      Paraview state file to preview results.
      |
      +- summary_of_cascade_all.csv
      |      Concatenated data of summary_of_cascade.csv for each cascade detail.
      |
      +- summary_of_cascade_each_atom_all.csv
      |      Concatenated data of summary_of_cascade_each_atom.csv for each cascade detail.
      |
      +- casc#####-gr####-num####
          |    Detail data for each cascade section. The section starts like,
          |    "Cascade     1:     Group    1      Number  1"
          |
          +- cascade_summary.json
          |      JSON packed data of cascade specific information
          |
          +- description_of_cascade.csv
          |      Table entitled with "Detailed Description of the Cascade part 1-3 of 3",
          |      which includes the initial and final position and other states of the
          |      knocked-on atoms
          |
          +- distant_iv_pairs.csv
          |      Table entitles with "Separations of Distant Interstitial-Vacancy Pairs".
          |      Interstitial and vacancy file numbers are related with those in
          |      description_of_cascade.csv and lattice_sites.csv.
          |      The coordinates of int. and vac. are also shown in this file.
          |
          +- lattice_sites.csv
          |      Table entitled with "Locations of the Cascade Lattice Sites", which includes
          |      the coordinate of lattice sites involved into the entire cascade process.
          |
          +- primary_recoil_ranges.csv
          |      Taken from "XX Primary Recoil Ranges" section, simple result on final status
          |      of primary recoil
          |
          +- summary_of_cascade.csv
          |      Taken from "Summary of Cascade  ####:  Group ### Number ###" section.
          |      This table includes summarized data of the single cascade on various numbers
          |      and energies
          |
          +- summary_of_cascade_each_atom.csv 
                 Taken from "Cascade Summary for XX atoms" section. 
                 This table shows decomposed data in summary_of_cascade.csv for each atom.

Notes
==================

Specify output directory (CLI only)
------------------------------------

The output directory may be specified by the second argument, such as 

.. code-block:: shell

  $$ ml_post marlowe_output.lst c:\temp\malrowe_results\20141201-01

Directory for cascade detetails (CLI only)
--------------------------------------------

In default, details of each cascade data is saved in
``OUTPUT/casc#####-gr#####-num#####`` directory. This can be modified by
``--cascade-directory-format`` option. The format should be compatible to `Python
Format String Syntax
<https://docs.python.org/3.4/library/string.html#formatstrings>`_.

==================================

`Return to Top <index.html>`_

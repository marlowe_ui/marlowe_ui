=====================
Marlowe input editor
=====================

:Author: Takaaki AOKI <aoki.takaaki@nagoya-u.ac.jp>
:Version: ${mui_version}
:Date: ${build_datestr}

.. contents:: Contents
   :depth: 1

Usage
=============================

[File][Save & Run]
----------------------

Save widget contents for both marlowe input data (usually having .dat extension) file and .json format.
The .json file can be loaded by [File][ .json] button.

When parameter file is saved correctly, then a dialog to run marlowe program will pop-up. User may run marlowe program by indicating proper path to marlowe executive and input data file.

[File][Load .json]
--------------------

Currently, mui.py allows to load data file in JSON format created by above [File][Save & Run] command.

The example files can be found at (mui install directory)/examples/

[Data][Dump to Console]
------------------------

Dump current widget contents to log window, which might be helpful on debugging.

[Data][Validate]
-----------------

[Data][Validate] menuoption tests widget contents (if a content of widget is not valid the widget turns red)
This validation routine also runs before [File][Save & Run] action.

.. note::

  Validation does not run automatically. The user should push
  [Validate] button explicitly to confirm the modification on GUI 
  is correct or not.


[Data][Clear]
-----------------

Set all parameters to default. It is noted that *default* does not means default values
explained in the original MARLOWE reference (chap. 8), but the values when MUI program starts. 

[Layout]
---------

THe MARLOWE input data consists of many complicated parameters. [Layout][Simple] reduces 
widgets, while [Layout][Full] shows all available widgets.


Bugs, issues, discussion for developers
=======================================

The author is pleased to here bug & issue reports and suggest & request for the software.

==================================

`Return to Top <index.html>`_

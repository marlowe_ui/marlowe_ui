"""
balloon help binder
"""


class BalloonHelpImplement(object):
    """Implementation of BalloonHelp, drived class should have following methods.
    """
    def bind_with_balloonhelp(self, b):
        """bind ballon (usually Pmw.Balloon()) to the widget of this class

        b is ballon instance implemented with b.bind(...)
        """
        pass

_version_major = 0
_version_minor = 5
_version_micro = 19

__version__ = "{0:d}.{1:d}.{2:d}".format(
    _version_major, _version_minor, _version_micro)

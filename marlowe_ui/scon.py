# scon.py
# size control parameters declared in marlowe calculation
# see chap. 4 of marlowe guide

import math

kind = 5
nlay = 5
nrbx = 2
root = math.sqrt(0.5)
sent = 4096.0
llcs = 25

# pass block?
nrnx = 4
